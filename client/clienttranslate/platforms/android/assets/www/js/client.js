'use strict';

//var socket = io.connect('http://192.168.1.105:8888'); // home network
//var socket = io.connect('https://192.168.20.38:8888'); //inspeedia2g
//var socket = io.connect('https://192.168.43.63:8888'); // tether
var socket = io.connect('http://192.168.20.53:8888'); // ubuntu server
//var socket = io.connect('https://192.168.1.103:8888', {secure: true}); // raspberry pi
//var socket = io.connect('http://192.168.1.105:8888'); // raspberry pi
//var stunTurnServer = 'stun:stun2.1.google.com:19302';
var stunTurnServer = 'numb.viagenie.ca';

var toDisplay, config, session, myConnection;
var myPeerConnection = null;    // RTCPeerConnection
var myUsername = null;
var targetUsername = null;      // To store username of other peer
var clientID = 0;
// To work both with and without addTrack() we need to note
// if it's available
var hasAddTrack = false;

// The media constraints object describes what sort of stream we want
// to request from the local A/V hardware (typically a webcam and
// microphone). Here, we specify only that we want both audio and
// video; however, you can be more specific. It's possible to state
// that you would prefer (or require) specific resolutions of video,
// whether to prefer the user-facing or rear-facing camera (if available),
// and so on.
//
// See also:
// https://developer.mozilla.org/en-US/docs/Web/API/MediaStreamConstraints
// https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
//
var mediaConstraints = {
    audio: true            // We want an audio track
    //video: true             // ...and we want a video track
};

var modal = document.getElementById('modal_confirm');
var span = document.getElementsByClassName('close')[0];
var msg = document.getElementById('modal_body_message');
var errMsg = document.getElementById('modal_footer_message');
var selectUser = document.getElementById('select_user');
var localAudio = document.getElementById('localAudio');
var remoteAudio = document.getElementById('remoteAudio');
var callPage = document.getElementById('callPage');

socket.on('connect', function(id) {
    //handle messages from the server
    socket.on('text', function(message) {
        //alert("Got message type " + message.type);
        var data = JSON.parse(message);

        switch(data.type) {
            case "id":
                clientID = data.id;
                setUsername();
                break;
            case "username":
                break;
            case "message":
                msg.innerHTML = data.msg;
                errMsg.innerHTML = "Enjoy listening";
                globalModal(data.msg);
                break;
            case "rejectusername":
                console.log("User rejected!");
                myUsername = data.username;
                break;
            case "offer":
                handleVideoOfferMsg(data);
                break;
            case "answer":
                handleVideoAnswerMsg(data);
                break;
            case "candidate":
                handleCandidate(data.candidate);
                break;
            case "leave":
                handleLeave();
                break;
            case "join":
                msg.innerHTML = data.msg;
                errMsg.innerHTML = "Enjoy listening";
                globalModal(data.msg);
                break;
            case "get_users":
                displayData(data.msg)
                break;
            case "error":
                msg.innerHTML = data.msg;
                errMsg.innerHTML = "An error occurred!";
                globalModal(data.msg);
                break;
            default:
                break;
        }
    });
});

function displayData(data) {
    var listElem = document.getElementById("userlistbox");

    // Remove all current list members. We could do this smarter,
    // by adding and updating users instead of rebuilding from
    // scratch but this will do for this sample.

    while (listElem.firstChild) {
        listElem.removeChild(listElem.firstChild);
    }

    // Add member names from the received list

    for (var index in data) {
        var item = document.createElement("li");
        item.appendChild(document.createTextNode(data[index]));
        item.addEventListener("click", invite, false);

        listElem.appendChild(item);
    }
}

// Create the RTCPeerConnection which knows how to talk to our
// selected STUN/TURN server and then uses getUserMedia() to find
// our camera and microphone and add that stream to the connection for
// use in our video call. Then we configure event handlers to get
// needed notifications on the call.

function createPeerConnection() {
    console.log("Setting up a connection...");

    // Create an RTCPeerConnection which knows to use our chosen
    // STUN server.

    myPeerConnection = new RTCPeerConnection({
        iceServers: [     // Information about ICE servers - Use your own!
            {
                urls: "turn:" + stunTurnServer,  // A TURN server
                username: "muazkh",
                credential: "webrtc@live.com"
            }
        ]
    });

    // Do we have addTrack()? If not, we will use streams instead.
    hasAddTrack = (myPeerConnection.addTrack !== undefined);

    // Set up event handlers for the ICE negotiation process.
    myPeerConnection.onicecandidate = handleICECandidateEvent;
    myPeerConnection.onnremovestream = handleRemoveStreamEvent;
    myPeerConnection.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
    myPeerConnection.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
    myPeerConnection.onsignalingstatechange = handleSignalingStateChangeEvent;
    myPeerConnection.onnegotiationneeded = handleNegotiationNeededEvent;

    // Because the deprecation of addStream() and the addstream event is recent,
    // we need to use those if addTrack() and track aren't available.

    if (hasAddTrack) {
        myPeerConnection.ontrack = handleTrackEvent;
    } else {
        myPeerConnection.onaddstream = handleAddStreamEvent;
    }
}

// Called by the WebRTC layer to let us know when it's time to
// begin (or restart) ICE negotiation. Starts by creating a WebRTC
// offer, then sets it as the description of our local media
// (which configures our local media stream), then sends the
// description to the callee as an offer. This is a proposed media
// format, codec, resolution, etc.

function handleNegotiationNeededEvent() {
    console.log("*** Negotiation needed");

    console.log("---> Creating offer");
        myPeerConnection.createOffer().then(function(offer) {
        console.log("---> Creating new description object to send to remote peer");
        return myPeerConnection.setLocalDescription(offer);
    }).then(function() {
        console.log("---> Sending offer to remote peer");
        var sendToServer = {
            name: myUsername,
            target: targetUsername,
            type: "offer",
            sdp: myPeerConnection.localDescription
        };
        socket.emit('message', JSON.stringify(sendToServer));
    }).catch(reportError);
}

// Called by the WebRTC layer when events occur on the media tracks
// on our WebRTC call. This includes when streams are added to and
// removed from the call.
//
// track events include the following fields:
//
// RTCRtpReceiver       receiver
// MediaStreamTrack     track
// MediaStream[]        streams
// RTCRtpTransceiver    transceiver
function handleTrackEvent(event) {
    console.log("*** Track event");
    //document.getElementById("received_video").srcObject = event.streams[0];
    //document.getElementById("hangup-button").disabled = false;
}

// Called by the WebRTC layer when a stream starts arriving from the
// remote peer. We use this to update our user interface, in this
// example.
function handleAddStreamEvent(event) {
    console.log("*** Stream added");
    //document.getElementById("received_video").srcObject = event.stream;
    //document.getElementById("hangup-button").disabled = false;
}

// An event handler which is called when the remote end of the connection
// removes its stream. We consider this the same as hanging up the call.
// It could just as well be treated as a "mute".
//
// Note that currently, the spec is hazy on exactly when this and other
// "connection failure" scenarios should occur, so sometimes they simply
// don't happen.
function handleRemoveStreamEvent(event) {
    console.log("*** Stream removed");
    closeVideoCall();
}

// Handles |icecandidate| events by forwarding the specified
// ICE candidate (created by our local ICE agent) to the other
// peer through the signaling server.
function handleICECandidateEvent(event) {
    if (event.candidate) {
        console.log("Outgoing ICE candidate: " + event.candidate.candidate);

        var sendToServer = {
            type: "new-ice-candidate",
            target: targetUsername,
            candidate: event.candidate
        };
        socket.emit('message', JSON.stringify(sendToServer));
    }
}

// Handle |iceconnectionstatechange| events. This will detect
// when the ICE connection is closed, failed, or disconnected.
//
// This is called when the state of the ICE agent changes.
function handleICEConnectionStateChangeEvent(event) {
    console.log("*** ICE connection state changed to " + myPeerConnection.iceConnectionState);

    switch(myPeerConnection.iceConnectionState) {
        case "closed":
        case "failed":
        case "disconnected":
            closeVideoCall();
            break;
    }
}

// Set up a |signalingstatechange| event handler. This will detect when
// the signaling connection is closed.
//
// NOTE: This will actually move to the new RTCPeerConnectionState enum
// returned in the property RTCPeerConnection.connectionState when
// browsers catch up with the latest version of the specification!

function handleSignalingStateChangeEvent(event) {
    console.log("*** WebRTC signaling state changed to: " + myPeerConnection.signalingState);
    switch(myPeerConnection.signalingState) {
        case "closed":
            closeVideoCall();
            console.log("closeVideoCall()");
            break;
    }
}

// Handle the |icegatheringstatechange| event. This lets us know what the
// ICE engine is currently working on: "new" means no networking has happened
// yet, "gathering" means the ICE engine is currently gathering candidates,
// and "complete" means gathering is complete. Note that the engine can
// alternate between "gathering" and "complete" repeatedly as needs and
// circumstances change.
//
// We don't need to do anything when this happens, but we log it to the
// console so you can see what's going on when playing with the sample.
function handleICEGatheringStateChangeEvent(event) {
    console.log("*** ICE gathering state changed to: " + myPeerConnection.iceGatheringState);
}

// Handle a click on an item in the user list by inviting the clicked
// user to video chat. Note that we don't actually send a message to
// the callee here -- calling RTCPeerConnection.addStream() issues
// a |notificationneeded| event, so we'll let our handler for that
// make the offer.
function invite(evt) {
    console.log("Starting to prepare an invitation");
    if (myPeerConnection) {
        alert("You can't start a call because you already have one open!");
    } else {
        var clickedUsername = evt.target.textContent;

        // Don't allow users to call themselves, because weird.

        if (clickedUsername === myUsername) {
            alert("I'm afraid I can't let you talk to yourself. That would be weird.");
            return;
        }

        // Record the username being called for future reference

        targetUsername = clickedUsername;
        console.log("Inviting user " + targetUsername);

        // Call createPeerConnection() to create the RTCPeerConnection.

        console.log("Setting up connection to invite user: " + targetUsername);
        createPeerConnection();

        // Now configure and create the local stream, attach it to the
        // "preview" box (id "local_video"), and add it to the
        // RTCPeerConnection.

        console.log("Requesting webcam access...");

        navigator.mediaDevices.getUserMedia(mediaConstraints).then(function(localStream) {
            console.log("-- Local video stream obtained");
            document.getElementById("localAudio").src = window.URL.createObjectURL(localStream);
            document.getElementById("localAudio").srcObject = localStream;

            if (hasAddTrack) {
                console.log("-- Adding tracks to the RTCPeerConnection");
                localStream.getTracks().forEach(track => myPeerConnection.addTrack(track, localStream));
            } else {
                console.log("-- Adding stream to the RTCPeerConnection");
                myPeerConnection.addStream(localStream);
            }
        }).catch(handleGetUserMediaError);
    }
}

// Called when the "id" message is received; this message is sent by the
// server to assign this login session a unique ID number; in response,
// this function sends a "username" message to set our username for this
// session.
function setUsername() {
    myUsername = document.getElementById("login_field").value;

    var userInfo = {
        type: "username",
        name: myUsername,
        date: Date.now(),
        id: clientID
    };
    console.log("USER INFORMATION = " + JSON.stringify(userInfo));
    socket.emit('message', JSON.stringify(userInfo));
}

// Accept an offer to video chat. We configure our local settings,
// create our RTCPeerConnection, get and attach our local camera
// stream, then create and send an answer to the caller.

function handleVideoOfferMsg(data) {
    var localStream = null;

    targetUsername = data.name;

    // Call createPeerConnection() to create the RTCPeerConnection.

    console.log("Starting to accept invitation from " + targetUsername);
    createPeerConnection();

    // We need to set the remote description to the received SDP offer
    // so that our local WebRTC layer knows how to talk to the caller.

    var desc = new RTCSessionDescription(data.sdp);

    myPeerConnection.setRemoteDescription(desc).then(function () {
        console.log("Setting up the local media stream...");
        return navigator.mediaDevices.getUserMedia(mediaConstraints);
    }).then(function(stream) {
        console.log("-- Local video stream obtained");
        localStream = stream;
        document.getElementById("localAudio").src = window.URL.createObjectURL(localStream);
        document.getElementById("localAudio").srcObject = localStream;

        if (hasAddTrack) {
            console.log("-- Adding tracks to the RTCPeerConnection");
            localStream.getTracks().forEach(track =>
                myPeerConnection.addTrack(track, localStream)
            );
        } else {
            console.log("-- Adding stream to the RTCPeerConnection");
            myPeerConnection.addStream(localStream);
        }
    }).then(function() {
        console.log("------> Creating answer");
        // Now that we've successfully set the remote description, we need to
        // start our stream up locally then create an SDP answer. This SDP
        // data describes the local end of our call, including the codec
        // information, options agreed upon, and so forth.
        return myPeerConnection.createAnswer();
    }).then(function(answer) {
        console.log("------> Setting local description after creating answer");
        // We now have our answer, so establish that as the local description.
        // This actually configures our end of the call to match the settings
        // specified in the SDP.
        return myPeerConnection.setLocalDescription(answer);
    }).then(function() {
        var msg = {
            name: myUsername,
            target: targetUsername,
            type: "video-answer",
            sdp: myPeerConnection.localDescription
        };

        // We've configured our end of the call now. Time to send our
        // answer back to the caller so they know that we want to talk
        // and how to talk to us.

        console.log("Sending answer packet back to other peer");
        sendToServer(msg);
    }).catch(handleGetUserMediaError);
}

// Responds to the "video-answer" message sent to the caller
// once the callee has decided to accept our request to talk.

function handleVideoAnswerMsg(data) {
    console.log("Call recipient has accepted our call");

    // Configure the remote description, which is the SDP payload
    // in our "video-answer" message.
    var desc = new RTCSessionDescription(data.sdp);
    myPeerConnection.setRemoteDescription(desc).catch(reportError);
}

// A new ICE candidate has been received from the other peer. Call
// RTCPeerConnection.addIceCandidate() to send it along to the
// local ICE framework.

function handleNewICECandidateMsg(data) {
    var candidate = new RTCIceCandidate(data.candidate);

    console.log("Adding received ICE candidate: " + JSON.stringify(candidate));
    myPeerConnection.addIceCandidate(candidate).catch(reportError);
}

/*
socket.on('disconnect', () => {
    socket.open();
});
*/

function globalModal(msg) {
    modal.style.display = "block";
}

span.onclick = function() {
    modal.style.display = "none";
}

function errorEmpty() {
    modal.style.display = "block";
}

function registerUser() {
    name = document.getElementById("login_field").value;
    if (name == "") {
        //alert("Cannot be empty!");
        msg.innerHTML = "Cannot be empty!";
        errMsg.innerHTML = "Try Again.";
        errorEmpty();
    } else {
        socket.emit('message', JSON.stringify({type: "join", name: name}));
        socket.emit('message', JSON.stringify({type: "login", name: name}));
        document.getElementById("login_field").value = "";
    }
}

function login_response(data) {
    if (data.error == data) {
        console.log("error");
    } else {}
}

function getUsers() {
    socket.emit('get_users');
}

function callUser() {
    var selectedUser = selectUser.options[selectUser.selectedIndex].value;
    var otherUsername = selectedUser;
    connectedUser = otherUsername;

    if (otherUsername.length > 0) {
        //make an offer
        myConnection.createOffer(function(offer) {
            console.log();
            socket.emit('message', JSON.stringify({type: "offer", offer: offer, name: connectedUser}));

            myConnection.setLocalDescription(offer);
        }, function (error) {
            alert("An error has occurred.");
        });
    }
}

function hangUpUser() {
    //send(JSON.stringify({type: "leave"}));
    closeVideoCall();
}

function closeVideoCall() {
    var remoteAudio = document.getElementById("remoteAudio");
    var localAudio = document.getElementById("localAudio");

    console.log("Closing the call");

    // Close the RTCPeerConnection

    if (myPeerConnection) {
        console.log("--> Closing the peer connection");

        // Disconnect all our event listeners; we don't want stray events
        // to interfere with the hangup while it's ongoing.

        myPeerConnection.onaddstream = null;  // For older implementations
        myPeerConnection.ontrack = null;      // For newer ones
        myPeerConnection.onremovestream = null;
        myPeerConnection.onnicecandidate = null;
        myPeerConnection.oniceconnectionstatechange = null;
        myPeerConnection.onsignalingstatechange = null;
        myPeerConnection.onicegatheringstatechange = null;
        myPeerConnection.onnotificationneeded = null;

        // Stop the videos

        if (remoteAudio.srcObject) {
            remoteAudio.srcObject.getTracks().forEach(track => track.stop());
        }

        if (localAudio.srcObject) {
            localAudio.srcObject.getTracks().forEach(track => track.stop());
        }

        remoteAudio.src = null;
        localAudio.src = null;

        // Close the peer connection

        myPeerConnection.close();
        myPeerConnection = null;
    }

    // Disable the hangup button
    targetUsername = null;
}

// Handle errors which occur when trying to access the local media
// hardware; that is, exceptions thrown by getUserMedia(). The two most
// likely scenarios are that the user has no camera and/or microphone
// or that they declined to share their equipment when prompted. If
// they simply opted not to share their media, that's not really an
// error, so we won't present a message in that situation.
function handleGetUserMediaError(e) {
    console.log(e);
    switch(e.name) {
        case "NotFoundError":
            alert("Unable to open your call because no camera and/or microphone" +
            "were found.");
            break;
        case "SecurityError":
        case "PermissionDeniedError":
            // Do nothing; this is the same as the user canceling the call.
            break;
        default:
            alert("Error opening your camera and/or microphone: " + e.message);
            break;
    }

    // Make sure we shut down our end of the RTCPeerConnection so we're
    // ready to try again.

    closeVideoCall();
}

function joinRoom(value) {
    socket.emit('message', JSON.stringify({type: value, room: value}));
}

function send(message) {
    // attach the other peer username to our message
    if (connectedUser) {
        message.name = connectedUser;
    }

    socket.send(JSON.stringify(message));
};

// Handles reporting errors. Currently, we just dump stuff to console but
// in a real-world application, an appropriate (and user-friendly)
// error message should be displayed.
function reportError(errMessage) {
    console.log("Error " + errMessage.name + ": " + errMessage.message);
}