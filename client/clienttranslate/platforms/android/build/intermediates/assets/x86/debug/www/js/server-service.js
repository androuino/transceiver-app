'use strict';
//import swal from 'sweetalert';

var ul = document.getElementById("client-number");
var start = document.getElementById("start");
var isServerStarted = false;
var sessions = [];
var clients = [];
// ~Warning~ You must get your own API Keys for non-demo purposes.
// ~Warning~ Get your PubNub API Keys: https://www.pubnub.com/get-started/
// The phone *number* can by any string value

let   session = null;
const number  = 77777;
const phone   = PHONE({
    number        : number,
    autocam       : false,
    publish_key   : 'pub-c-8be964fa-5063-4f39-8411-0b4a50880626',
    subscribe_key : 'sub-c-b30f852e-46d6-11e8-91e7-8ad1b2d46395',
    media         : {audio : true, video : false},
    ssl           : true
});

// initial the element's state
phone.$('startcall').disabled = true;
phone.$('stopcall').disabled = true;
phone.$('startcam').style.backgroundColor = "#ff0000";
phone.$('dial').disabled = true;

// Debugging Output
phone.debug( info => console.info(info) );

// Show Number
phone.$('number').innerHTML = 'Number: ' + number;

// Start Camera
phone.bind('mousedown,touchstart', phone.$('startcam'), function() {
    phone.camera.start();
    phone.$('startcam').innerHTML = "Server Started"
    phone.$('startcam').style.backgroundColor = "#00ff00";
    phone.$('dial').disabled = false;
    isServerStarted = true;
});

// Stop Camera
//phone.bind('mousedown,touchstart', phone.$('stopcam'), event => phone.camera.stop());

// Local Camera Display
phone.camera.ready( video => {
    //phone.$('video-out').appendChild(video);
});

// dialing multiple clients
/*sessions.push(phone.dial(phone.$('dial').value));
sessions.push(phone.dial('123'));
sessions.push(phone.dial('5123'));
sessions.push(phone.dial('4422'));
sessions.push(phone.dial('2013'));

sessions.foreach(function(clients){
    clients.connected(function(session){});
    clients.ended(function(session){});
});*/

// As soon as the phone is ready we can make calls
phone.ready(()=>{
    // Start Call
    phone.bind('mousedown,touchstart', phone.$('startcall'), function(){
        if (phone.$('dial').value == "" || !isServerStarted) {
            swal({
                title : "Error!",
                text  : "Number is empty or server is down.",
                icon  : "warning",
            });
        } else {
            session = phone.dial(phone.$('dial').value);
            phone.$('dial').value = "";
            phone.$('startcall').disabled = false;
            phone.$('stopcall').disabled = false;
            phone.$('startcam').disabled = true;
        }
    });

    // Stop Call
    phone.bind('mousedown,touchstart', phone.$('stopcall'), function(){
        phone.hangup();
        phone.$('startcall').disabled = true;
        phone.$('stopcall').disabled = true;
        phone.$('startcam').disabled = false;
        phone.$('startcam').innerHTML = "Start The Server";
        phone.$('startcam').style.backgroundColor = "#ff0000";
        phone.$('dial').disabled = true;
        isServerStarted = false;
    });
});

// Display client's number
phone.message(function(session, message){
    clients = [];
    clients.push(message.text);
    for (var i = 0; i < clients.length; i++) {
    }
    var li = document.createElement("li");
    li.appendChild(document.createTextNode("User " + message.text));
    ul.appendChild(li);
});

// When Call Comes In or is to be Connected
phone.receive(function(session){
    // Display Your Friend's Live Video
    session.connected(function(session){
        phone.$('video-out').appendChild(session.video);
        //if (session.number == )
    });
    session.ended(function(session) {
        if (number != session.number) {
            //alert("User " + session.number + " hang up!");
            swal("User " + session.number + " hang up!");
        }
    });
});

