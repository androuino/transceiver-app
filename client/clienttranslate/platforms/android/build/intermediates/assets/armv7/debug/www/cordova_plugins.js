cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "WebViewer.webview",
    "file": "plugins/WebViewer/www/webview.js",
    "pluginId": "WebViewer",
    "clobbers": [
      "webview"
    ]
  },
  {
    "id": "cordova-plugin-preventsleep.client",
    "file": "plugins/cordova-plugin-preventsleep/www/client.js",
    "pluginId": "cordova-plugin-preventsleep",
    "clobbers": [
      "community.preventsleep"
    ]
  },
  {
    "id": "cordova-plugin-websocket.websocket",
    "file": "plugins/cordova-plugin-websocket/www/websocket.js",
    "pluginId": "cordova-plugin-websocket",
    "clobbers": [
      "WebSocket"
    ]
  },
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-bit6.Utils",
    "file": "plugins/cordova-plugin-bit6/www/utils.js",
    "pluginId": "cordova-plugin-bit6"
  },
  {
    "id": "cordova-plugin-bit6.Bit6SDK",
    "file": "plugins/cordova-plugin-bit6/www/bit6.js",
    "pluginId": "cordova-plugin-bit6"
  },
  {
    "id": "cordova-plugin-bit6.PushWrappers",
    "file": "plugins/cordova-plugin-bit6/www/push-wrappers.js",
    "pluginId": "cordova-plugin-bit6"
  },
  {
    "id": "cordova-plugin-bit6.Bit6",
    "file": "plugins/cordova-plugin-bit6/www/index-bit6.js",
    "pluginId": "cordova-plugin-bit6",
    "clobbers": [
      "Bit6"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "WebViewer": "0.0.1",
  "cordova-plugin-console": "1.1.0",
  "cordova-plugin-crosswalk-webview": "2.3.0",
  "cordova-plugin-legacy-whitelist": "1.1.2",
  "cordova-plugin-preventsleep": "1.1.1",
  "cordova-plugin-websocket": "0.12.2",
  "cordova-plugin-whitelist": "1.3.2",
  "cordova-android-support-gradle-release": "1.3.0",
  "cordova-plugin-device": "2.0.2",
  "cordova-plugin-bit6": "0.11.0",
  "cordova-plugin-iosrtc": "4.0.2"
};
// BOTTOM OF METADATA
});