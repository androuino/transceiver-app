'use strict';

// ~Warning~ You must get your own API Keys for non-demo purposes.
// ~Warning~ Get your PubNub API Keys: https://www.pubnub.com/get-started/
// The phone *number* can by any string value

let   session = null;
const number  = Math.ceil(Math.random()*10000);
const phone   = PHONE({
    number        : number,
    autocam       : false,
    publish_key   : 'pub-c-2f72ae3a-aa73-40c1-9333-aedcea1832c7',
    subscribe_key : 'sub-c-88efc308-ea1a-11e7-9723-66707ad51ffc'
});

// Debugging Output
phone.debug( info => console.info(info) );

// Show Number
phone.$('number').innerHTML = 'Number: ' + number;

// Start Camera
phone.bind('mousedown,touchstart', phone.$('startcam'), event => phone.camera.start());

// Stop Camera
phone.bind('mousedown,touchstart', phone.$('stopcam'), event => phone.camera.stop());

// Local Camera Display
phone.camera.ready( video => {
    phone.$('video-out').appendChild(video);
});

// As soon as the phone is ready we can make calls
phone.ready(()=>{
    // Start Call
    phone.bind('mousedown,touchstart', phone.$('startcall'), event => session = phone.dial(phone.$('dial').value));
    // Stop Call
    phone.bind('mousedown,touchstart', phone.$('stopcall'), event => phone.hangup());
});

// When Call Comes In or is to be Connected
phone.receive(function(session){
    // Display Your Friend's Live Video
    session.connected(function(session){
        phone.$('video-out').appendChild(session.video);
    });
});
