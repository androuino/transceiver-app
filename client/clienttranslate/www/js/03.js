var video_out = document.getElementById("vid-box");
var caller = document.getElementById("caller");

function login(form) {
	var phone = window.phone = PHONE({
	    number        : form.username.value || "Anonymous", // listen on username line else Anonymous
	    publish_key   : 'pub-c-8be964fa-5063-4f39-8411-0b4a50880626',
	    subscribe_key : 'sub-c-b30f852e-46d6-11e8-91e7-8ad1b2d46395',
	    media         : {audio : true, video : true},
	    ssl           : true
	});
	phone.ready(function(){ form.username.style.background="#55ff5b"; });
	phone.receive(function(session){
	    session.connected(function(session) {
	         video_out.appendChild(session.video);
	         caller.innerHTML=session.number;
	     });
	    session.ended(function(session) { video_out.innerHTML=''; });
	});
	return false; // So the form does not submit.
}

function makeCall(form){
	if (!window.phone) alert("Login First!");
	else phone.dial(form.number.value);
	return false;
}