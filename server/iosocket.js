'use strict';

var fs = require('fs');
var https = require('https');
var http = require('http');

var express = require('express');
var app = express();

/*var httpsOptions = {
    key: fs.readFileSync("/etc/pki/tls/certs/mdn.key"),
    cert: fs.readFileSync("/etc/pki/tls/certs/mdn.crt"),
    requestCert: true
};*/

const httpServer = http.createServer(app);
//const httpsServer = https.createServer(httpsOptions, app);

const io = require('socket.io')(httpServer, {
	path: '',
	serveClient: false,
	pingInterval: 10000,
	pingTimeout: 5000,
	cookies: false
});

var mysql = require('mysql');
var conn = mysql.createConnection({
	connectionLimit 	: 100,
	host            	: 'localhost',
	user            	: 'root',
	password        	: 'root', //'jmoreno30'
	database        	: 'usersdb',
	port            	: '3306'
});

conn.connect();

var usersList = [], data, type, connectionArray = [];
var nextID = Date.now();
var appendToMakeUnique = 1;

function originIsAllowed(origin) {
    return true;    // We will accept all connections
}

io.on('connection', function(socket) {
	console.log('client connected ' + socket.id);

    socket.on('request', function(request) {
        console.log('on request');
        if (!originIsAllowed(request.origin)) {
            request.reject();
            console.log("Connection from " + request.origin + " rejected.");
            return;
        }

        // Accept the request and get a connection.
        var connection = request.accept("json", request.origin);

        // Add the new connection to our list of connections.

        console.log("Connection accepted from " + connection.remoteAddress + ".");
        connectionArray.push(connection);
        console.log("VALUE OF connectionArray = " + connectionArray);

        connection.clientID = nextID;
        nextID++;

        var msg = {
            type: "id",
            id: connection.clientID
        };
        socket.emit('text', JSON.stringify(msg));
    });

    var sendToClients = true;
    var connect = getConnectionForID(socket.id);

	//handle messages from the server
	socket.on('message', function(message) {

		//accepting only JSON messages
		try {
			data = JSON.parse(message);
		} catch (e) {
			console.log("Invalid JSON");
			data = {};
		}

		switch(data.type) {
			case "join":
				onJoin(socket, data.name);
				break;
			case "english":
				try {
					console.log('joined to room : ', data.room);
					socket.join(data.room);
					io.sockets.to(data.room).emit('text', JSON.stringify({type: 'message', msg: 'English translator on line.'}));
				} catch (e) {
					console.log('error', 'join room: ', e);
					socket.emit('text', JSON.stringify({type: 'error', msg: 'could not perform requested action.'}));
				}
				break;
			case "japanese":
				try {
					console.log('joined room : ', data.room);
					socket.join(data.room);
					io.sockets.to(data.room).emit('text', JSON.stringify({type: 'message', msg: 'Nihongo translator on line.'}));
				} catch (e) {
					console.log('error', 'join room: ', e);
					socket.emit('text', JSON.stringify({type: 'error', msg: 'could not perform requested action.'}));
				}
				break;
			case "spanish":
				try {
					console.log('joined room : ', data.room);
					socket.join(data.room);
					io.sockets.to(data.room).emit('text', JSON.stringify({type: 'message', msg: 'Spanish translator on line.'}));
				} catch (e) {
					console.log('error', 'join room: ', e);
					socket.emit('text', JSON.stringify({type: 'error', msg: 'could not perform requested action.'}));
				}
				break;
			case "portuguese":
				try {
					console.log('joined room : ', data.room);
					socket.join(data.room);
					io.sockets.to(data.room).emit('text', JSON.stringify({type: 'message', msg: 'Portuguese translator on line.'}));
				} catch (e) {
					console.log('error', 'join room: ', e);
					socket.emit('text', JSON.stringify({type: 'error', msg: 'could not perform requested action.'}));
				}
				break;
			case "message":
			    data.name = connect.username;
			    break;
			case "username":
                var nameChanged = false;
                var origName = data.name;

                // Ensure the name is unique by appending a number to it
                // if it's not; keep trying that until it works.
                while (!isUsernameUnique(data.name)) {
                    data.name = origName + appendToMakeUnique;
                    appendToMakeUnique++;
                    nameChanged = true;
                }

                // If the name had to be changed, we send a "rejectusername"
                // message back to the user so they know their name has been
                // altered by the server.
                if (nameChanged) {
                    var changeMsg = {
                        id: msg.id,
                        type: "rejectusername",
                        name: data.name
                    };
                    socket.emit('text', JSON.stringify(changeMsg));
                }

                // Set this connection's final username and send out the
                // updated user list to all users. Yeah, we're sending a full
                // list instead of just updating. It's horribly inefficient
                // but this is a demo. Don't do this in a real app.
                connect.username = data.name;
                sendToClients = false;  // We already sent the proper responses
                break;
			default:
				socket.emit('text', JSON.stringify({type: "error", msg: "Command not found: " + data}));
				break;
		}

        if (sendToClients) {
            var msgString = JSON.stringify(data);
            var i;

            // If the message specifies a target username, only send the
            // message to them. Otherwise, send it to every user.
            if (data.target && data.target !== undefined && data.target.length !== 0) {
                sendToOneUser(data.target, msgString);
            } else {
                for (i=0; i<connectionArray.length; i++) {
                    connectionArray[i].sendUTF(msgString);
                }
            }
        }
    });

	socket.on('join', function(data) {
		var checkIfUserExist = "SELECT username FROM users WHERE `username`=?";
		conn.query(checkIfUserExist, data, function(err, row) {
			try {
				if (err) {
					throw err;
				}

				if (row.length > 0) {
					socket.emit('text', JSON.stringify({type: 'login', name: data}));
					//return;
				} else {
					var query = "INSERT INTO users (`username`) VALUES ('"+data+"')";
					conn.query(query, function(err, results) {
						try {
							if (err) {
								throw err;
							} else {
								socket.emit('text',   {type: 'join', msg: 'You are registered ' + data + ", welcome to the Family.", success: true});
								console.log('Client added and inserted into database');
							}
						} catch (ee) {
							console.log(ee);
						}
					});
				}
			} catch (eee) {
				console.log(eee);
			}
		});
	});

	socket.on('get_users', function() {
		console.log("Admin wants to get the list of all registered users");
		usersList = [];
		var query = "SELECT username FROM users";
		var convert;
		conn.query(query, function(err, results) {
			try {
				if (err) {
					throw err;
				} else {
					convert = JSON.stringify(results);
					results.forEach((results) => {
						usersList.push(`${results.username}`);
					});
					console.log(usersList);
					socket.emit('text', JSON.stringify({type: 'get_users', msg: usersList}));
					//socket.emit('text', {type: 'get_users', msg: usersList});
				}
			} catch (e) {
				console.log(e);
			}
		});
	});

	//when user exits, for example closes a browser window
	//this may help if we are still in "offer","answer" or "candidate" state
	socket.on("close", function() {

		if(socket.name) {
			delete users[socket.name];

			if(socket.otherName) {
				console.log("Disconnecting from ", socket.otherName);
				var conn = users[socket.otherName];
				conn.otherName = null;

				if(conn != null) {
					socket.emit(conn, JSON.stringify({type: "leave"}));
				}
			}
		}
	});

	try {
		socket.on('disconnect', function() {
			console.log('client disconnected');
		});
	} catch (e) {
		socket.emit(JSON.stringify({type: "error", msg: e}));
	}
});

//httpsServer.listen(8888);
httpServer.listen(8888);

// Scan the list of connections and return the one for the specified
// clientID. Each login gets an ID that doesn't change during the session,
// so it can be tracked across username changes.
function getConnectionForID(id) {
    var connect = null;
    var i;

    for (i=0; i<connectionArray.length; i++) {
        if (connectionArray[i].clientID === id) {
            connect = connectionArray[i];
            break;
        }
    }

    return connect;
}

// Sends a message (which is already stringified JSON) to a single
// user, given their username. We use this for the WebRTC signaling,
// and we could use it for private text messaging.
function sendToOneUser(target, msgString) {
    var isUnique = true;
    var i;

    for (i=0; i<connectionArray.length; i++) {
        if (connectionArray[i].username === target) {
            connectionArray[i].sendUTF(msgString);
            break;
        }
    }
}

// Scans the list of users and see if the specified name is unique. If it is,
// return true. Otherwise, returns false. We want all users to have unique
// names.
function isUsernameUnique(name) {
    var isUnique = true;
    var i;

    for (i=0; i<connectionArray.length; i++) {
        if (connectionArray[i].username === name) {
            isUnique = false;
            break;
        }
    }
    return isUnique;
}

function onJoin(socket, data) {
	var checkIfUserExist = "SELECT username FROM users WHERE `username`=?";
	conn.query(checkIfUserExist, data, function(err, row) {
		try {
			if (err) {
				throw err;
			}

			if (row.length > 0) {
				socket.emit('text', JSON.stringify({type: 'login', name: data}));
				//return;
			} else {
				var query = "INSERT INTO users (`username`) VALUES ('"+data+"')";
				conn.query(query, function(err, results) {
					try {
						if (err) {
							throw err;
						} else {
							socket.emit('text',   JSON.stringify({type: 'join', msg: 'You are registered ' + data + ", welcome to the Family.", success: true}));
							console.log('Client added and inserted into database');
						}
					} catch (ee) {
						console.log(ee);
					}
				});
			}
		} catch (eee) {
			console.log(eee);
		}
	});
}
