"use strict";

var express = require('express');
var https = require('https');
var pem = require('pem');

pem.createCertificate({days: 1, selfSigned: true}, function(err, keys) {
	var options = {
		key: keys.serviceKey,
		cert: keys.certificate
	};

	var app = express();

	app.use(express.static('../'));

	https.createServer(options, app).listen(8888);

	console.log('listening on 8888');
});
